\documentclass[a4paper,10pt]{book}


\newcommand{\currentVersion}{1.2}


\usepackage{fullpage}
\usepackage[pdftex]{hyperref}
%\usepackage[utf8]{inputenc}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
%\usepackage{aeguill}
\usepackage{frenchle}
\usepackage{graphicx}
\usepackage{color}
\usepackage{xcolor}
\usepackage{minted}
\definecolor{codebg}{rgb}{0.96,0.96,0.96}

%\usepackage{lastpage}
%\usepackage{pageslts}

\newcommand{\fixme}[1]{\textcolor{red}{\textbf{FIXME}: #1}}
%\newcommand{\footnoteURL}[1]{\footnote{\href{#1}{\texttt{#1}}}}
\newcommand{\footnoteURL}[1]{\footnote{\url{#1}}}
\newcommand{\footnoteURLlabel}[2]{\footnote{\href{#1}{\texttt{#2}}}}

%\newtheorem{etape}{Step}
\newtheorem{etape}{�tape}



\title{Sequence Alignment}
%\date{19 d�cembre 2018 -- Dur�e 2 heures}
\author{Wesley Delage, Marine Louarn and Olivier Dameron}

%\renewcommand{\thesection}{\arabic{section}}

\begin{document}

\begingroup
\let\newpage\relax%
\maketitle

Version~\textbf{\currentVersion}

\vspace{0.5cm}

The latest version of this document is available at:\\
\url{https://gitlab.com/odameron/sequencealignmentcourse}
\endgroup


\chapter{Contexte}

\section{Objectif}

L'objectif de ce projet est d'aborder le probl�me de l'alignement de s�quences, qui consiste � \textbf{identifier les portions similaires de deux s�quences}. 
On en profitera pour pr�senter la programmation dynamique, et se familiariser avec les algorithmes classiques de Needlman et Wunsch (alignement global) et de Smith et Waterman (alignement local).



\section{Principes d'alignement de s�quences}
\label{sec:principesAlignement}

\paragraph{S�quences}
Pour le principe, une s�quence est une liste (�ventuellement avec r�p�titions) de lettres prises parmi un alphabet.
En pratique, on s'int�ressera particuli�rement aux \emph{s�quences biologiques}, qui englobent~:
\begin{itemize}
 \item les \emph{s�quences de nucl�otides} (A, C, G et T pour de l'ADN, et A, C, G, U pour de l'ARN)~;
 \item les \emph{s�quences d'acides amin�s} (22 classiques) pour les prot�ines.
\end{itemize}


\paragraph{Mutations (de s�quences)}
Au cours de l'�volution biologique, une s�quence d'ADN change, le plus souvent en raison d'erreurs al�atoires dans la r�plication ou la copie d'une s�quence d'ADN. 
Ces erreurs de r�plication sont appel�es \emph{mutations}. 
Les diff�rents �v�nements de mutation qui peuvent survenir sont~:
  \begin{itemize}
  \item des \emph{substitutions}~: o� un nucl�otide (ou un acide amin�, dans les s�quences prot�iques) est remplac� par un autre~;
  \item des \emph{insertions}~: lorsqu'un ou plusieurs nucl�otides contigus sont ins�r�s dans une s�quence~;
  \item des \emph{d�l�tions}~: lorsqu'un ou plusieurs nucl�otides contigus sont supprim�s d'une s�quence.
  \end{itemize}
  
  Lorsque l'on compare deux s�quences, une d�l�tion sur l'une peut aussi �tre interpr�t�e comme une insertion sur l'autre.
  S'il n'y a pas lieux de privil�gier une s�quence par rapport � l'autre, on utilise parfois le terme g�n�rique \emph{indel} pour �~insertion-d�l�tion~�.
  
  
  \paragraph{Alignement de s�quences}
L'objectif de l'alignement de s�quences est, � partir de deux s�quences, de g�n�rer une hypoth�se sur les positions de s�quences d�riv�es d'une position de s�quence ancestrale commune. 
En pratique, nous d�veloppons cette hypoth�se en comparant les deux s�quences et en ins�rant des gaps si n�cessaires afin de maximiser leur(s) sous-s�quence(s)\footnote{\textbf{Sous-s�quence~:} s�quence qui peut �tre d�riv�e d'une autre s�quence en y supprimant z�ro, un ou plusieurs �l�ments sans changer l'ordre des �l�ments restants. Contrairement � la notion de sous-cha�ne, les �l�ments de la sous-s�quence ne sont pas n�cessairement cons�cutifs dans la s�quence originale.} commune(s).
%En pratique, nous d�veloppons cette hypoth�se en alignant les s�quences les unes sur les autres en ins�rant des gaps, si n�cessaire, d'une mani�re qui maximise leur similarit�. 
C'est une approche de parcimonie maximale, o� nous supposons que l'explication la plus simple (celle impliquant le moins d'�v�nements de mutation extr�me) est la plus probable.
  
L'alignement de s�quences sert, entre autres, � identifier des fonctions de proteines. 
Le r�le des proteines �tant essentiel, on part du principe que deux proteines de s�quences similaires auront des propri�t� similaires. 
On peut donc rapprocher la s�quence de proteines inconnues � celles connues et en d�duire leur fonction.


En comparant les s�quences \texttt{AGCTGCTATGATACGACGAT} et \texttt{ATCATATACCGACGAT}, on repr�sente un alignement de la mani�re suivante (15 matchs, 6 indels et 0 substitution)~:
\begin{samepage}
 \begin{verbatim}
AGCTGCTATGATAC-GACGAT
A--T-C-AT-ATACCGACGAT
 \end{verbatim}
\end{samepage}



Remarquez que pour ces deux s�quences, l'alignement n'est pas unique et on aurait aussi pu avoir 14 matchs, 6 indels et une substitution (� la seconde position)~:
\begin{samepage}
 \begin{verbatim}
AGCTGCTATGATA-CGACGAT
ATC----AT-ATACCGACGAT
 \end{verbatim}
\end{samepage}



Voir m�me dans le pire des cas 0 matchs, 36 indels et 0 substitution~:
\begin{samepage}
 \begin{verbatim}
AGCTGCTATGATACGACGAT----------------
--------------------ATCATATACCGACGAT
 \end{verbatim}
\end{samepage}



\section{Approche na�ve~: limites de la r�cursivit�}

Trouver un alignement optimal (c'est-�-dire avec le plus de matches et le moins d'indels et de substitutions~; il n'est pas forc�ment unique) n�cessite d'examiner toutes les configurations possibles.
Comme souvent pour des op�rations sur les cha�nes et les listes, la programmation r�cursive offre une solution �l�gante.
Par contre, on va voir qu'une limitation majeure est que dans le cas de l'alignement de s�quences, l'ex�cution fait calculer plusieurs fois la m�me chose.

Pour aligner $(x_i)_{i=0}^n$ et $(y_j)_{j=0}^m$, il faut comparer~:
\begin{itemize}
 \item d�terminer si $(x_0, y_0)$ est un match ($x_0 = y_0$) ou une mutation ($x_0 \neq y_0$) et rappeler l'alignement sur $(x_i)_{i=\textbf{1}}^n$ et $(y_j)_{j=\textbf{1}}^m$~;
 \item consid�rer que $x_0$ est un indel et rappeler l'alignement sur $(x_i)_{i=\textbf{1}}^n$ et $(y_j)_{j=0}^m$~;
 \item consid�rer que $y_0$ est un indel et rappeler l'alignement sur $(x_i)_{i=0}^n$ et $(y_j)_{j=\textbf{1}}^m$~;
\end{itemize}


\begin{etape}
 \label{step:repetitionComparaison}
 On veut comparer les s�quences \texttt{ABC} et \texttt{abc}. Dessinez l'arbre d'ex�cution (inspirez-vous de la figure~\ref{fig:recursiveSequenceAlignment}). 
 Combien de fois la comparaison de \texttt{BC} et \texttt{bc} est-elle effectu�e~? 
 %�videmment, plus les s�quences � comparer sont longues, et plus on effectue les m�mes comparaisons...
\end{etape}

\begin{figure}[h!]
 \centering
 \includegraphics[height=5cm,keepaspectratio=true]{figures/recursiveSequenceAlignment.png}
 % recursiveSequenceAlignment.pdf: 842x595 px, 72dpi, 29.70x20.99 cm, bb=0 0 842 595
 \caption{D�but de l'arbre d'ex�cution d'une fonction r�cursive d'alignement de s�quences. Tant qu'une des s�quences n'est pas vide, on examine trois possibilit�s : une fl�che vers la gauche indique que l'on consid�re que le premier caract�re de la premi�re s�quence est une insertion, fl�che vers la droite c'est le premier caract�re de la seconde s�quence qui est une insertion, et fl�che vers le bas on compare le premier caract�re de la premi�re s�quence au premier caract�re de la seconde s�quence (s'ils sont identiques c'est un match, et sinon une substitution).}
 \label{fig:recursiveSequenceAlignment}
\end{figure}


\begin{etape}
 \textsc{(optionnelle)} �crivez une fonction r�cursive d'alignement. Quel est l'alignement optimal (ou les alignements optimaux) des s�quences \texttt{AGCTGCTATGATACGACGAT} et \texttt{ATCATATACCGACGAT} vue � la fin de la section~\ref{sec:principesAlignement}~?
\end{etape}

�videmment, plus les s�quences � comparer sont longues, et plus on effectue les m�mes comparaisons, ce qui est extr�mement p�nalisant dans le cas des s�quences biologiques.
On reviendra � la section~\ref{sec:dynamicProgramming} sur une strat�gie pour �viter d'effectuer plusieurs fois les m�mes �tapes interm�diaires.





\chapter{Global sequence alignment: Needleman and Wunsch}
\label{chap:globalAlignment}


\section{Matrice de comparaison de deux s�quences}
\label{sec:matriceDeComparaisonDeSequences}

\paragraph{Matrice de comparaison}

Pour comparer une s�quence de longueur $n$ et une s�quence de longueur $m$ de fa�on exhaustive, il faut effectuer $n \times m$ comparaisons de lettres.

Une fa�on intuitive de visualiser toutes ces combinaisons est d'utiliser une matrice de $n$ lignes et de $m$ colonnes.
La $i^{eme}$ ligne correspond � la $i^{eme}$ lettre de la premi�re s�quence, et la $j^{eme}$ colonne correspond � la $j^{eme}$ lettre de la seconde s�quence.

\begin{etape}
 \label{step:tracerMatrice}
 On souhaite comparer les s�quences \texttt{AACT} et \texttt{CAAGT}. Dessinez la matrice de comparaison.
\end{etape}




\paragraph{Parcours de la matrice}

Comparer deux s�quences consiste � les parcourir chacune de la premi�re � la derni�re lettre en se d�pla�ant toujours de la gauche vers la droite.
Dans la matrice, cela consiste donc �~:
\begin{itemize}
 \item partir du coin sup�rieur (premi�re lettre de la premi�re s�quence) gauche (premi�re lettre de la seconde s�quence)~;
 \item aller au coin inf�rieur (derni�re lettre de la premi�re s�quence) droit (derni�re lettre de la seconde s�quence)~;
 \item en se d�pla�ant de mani�re monotone soit~:
 \begin{itemize}
  \item \textbf{d'une case vers le bas~:} on passe ainsi � la lettre suivante dans la premi�re s�quence et on ne change pas de position dans la seconde s�quence, ce qui revient � consid�rer que la lettre que l'on vient de quitter est une insertion dans la premi�re s�quence (ou une d�l�tion dans la seconde s�quence). On ins�re alors un �~\texttt{-}~� apr�s la position courante dans la seconde s�quence.
  \item \textbf{d'une case vers la droite~:} on ne change pas la position dans la premi�re s�quence, et on passe � la lettre suivante dans la seconde s�quence, ce qui revient � consid�rer que la lettre que l'on vient de quitter est une insertion dans la seconde s�quence. On ins�re alors un �~\texttt{-}~� apr�s la position courante dans la premi�re s�quence.
  \item \textbf{en diagonale vers la case inf�rieure droite~:} on passe alors � la lettre suivante dans chacune des deux matrices. Si les lettres que l'on vient de quitter sont les m�mes, c'�tait un match~; sinon c'�tait une substitution.
 \end{itemize}
\end{itemize}

\begin{etape}
 Dans la matrice de l'�tape~\ref{step:tracerMatrice}, donner les alignements correspondant aux parcours suivants (les couples $(x,y)$ repr�sentent les coordonn�es des cases de la matrice de comparaison) en comptant pour chacun le nombre de matchs, le nombre d'indels et le nombre de substitutions~:
 \begin{itemize}
  \item $(0,0) \rightarrow (0,1) \rightarrow (1,2) \rightarrow (2,3) \rightarrow (3,3) \rightarrow (3,4)$
  \item $(0,0) \rightarrow (0,1) \rightarrow (1,2) \rightarrow (2,3) \rightarrow (2,4) \rightarrow (3,4)$
  \item $(0,0) \rightarrow (0,1) \rightarrow (1,2) \rightarrow (2,3) \rightarrow (3,4)$
  \item $(0,0) \rightarrow (0,1) \rightarrow (0,2) \rightarrow (0,3) \rightarrow (0,4) \rightarrow (1,4) \rightarrow (2,4) \rightarrow (3,4)$
  \item $(0,0) \rightarrow (1,0) \rightarrow (2,0) \rightarrow (3,0) \rightarrow (3,1) \rightarrow (3,2) \rightarrow (3,3) \rightarrow (3,4)$
 \end{itemize}
\end{etape}

On constate que l'ensemble des chemins du coin sup�rieur gauche de la matrice de comparaison � son coin inf�rieur droit selon les trois d�placement possibles correspond � l'ensemble des comparaisons des deux s�quences.
\textbf{L'objectif est maintenant d'identifier parmi ces chemins celui ou ceux qui maximisent le nombre de matchs en minimisant le nombre d'indels et de substitutions.}

On constate �galement que pour une case qui n'est pas sur la premi�re ou la derni�re ligne, ni sur la premi�re ou la derni�re colonne~:
\begin{itemize}
 \item il y a plusieurs chemins qui partent du coin sup�rieur gauche et qui aboutissent � cette case~;
 \item il y a plusieurs chemins qui partent de cette case et qui vont au coin inf�rieur droit.
\end{itemize}

On voit ainsi se dessiner la combinatoire de tous les chemins qui relient le coin sup�rieur gauche au coin inf�rieur droit et qui passent par cette case (et il nous faudra ensuite faire de m�me pour toutes les autres case).
Parmi tous ces chemins reliant les deux coins, le ou les chemins optimaux empruntent tous le chemin optimal (ou l'un des chemin optimaux) reliant la case au coin inf�rieur droit.
Pour tous les chemins qui partent du coin sup�rieur gauche, une fois qu'ils arrivent � la case consid�r�e il ne devrait donc plus �tre n�cessaire d'explorer � nouveau tous les chemins reliant la case au coin inf�rieur droit.
Remarquez le parall�le avec la situation de l'�tape~\ref{step:repetitionComparaison}.
On ne fera pas l'�conomie d'explorer toutes les cases, mais contrairement � l'approche r�cursive, on va chercher un algorithme qui ne recalcule pas � chaque fois qu'il arrive sur une case le chemin optimal de cette case au coin inf�rieur droit.



\section{Programmation dynamique pour ne pas r�p�ter les �tapes interm�diaires}
\label{sec:dynamicProgramming}

La \emph{programmation dynamique}\footnoteURL{https://fr.wikipedia.org/wiki/Programmation_dynamique} est une m�thode pour r�soudre les probl�mes d'optimisation (�a tombe bien, on cherche justement � maximiser le nombre de matchs et � minimiser le nombre de mutations).
Elle consiste � r�soudre un probl�me en le d�composant en sous-probl�mes, puis � r�soudre les sous-probl�mes, des plus petits aux plus grands en stockant les r�sultats interm�diaires.
Elle a notamment �t� appliqu�e � plusieurs probl�mes classiques en informatique comme le probl�me du plus court chemin, au probl�me du sac � dos, au probl�me du rendu de monnaie, au calcul de la distance de Levenstein.

Pour l'alignement de s�quences, le dernier paragraphe de la section~\ref{sec:matriceDeComparaisonDeSequences} expliquait que le probl�me de trouver les chemins optimaux reliant le coin sup�rieur gauche de la matrice de comparaison � son coin inf�rieur droit pouvait �tre d�compos� en autant de sous-probl�mes qu'il y a de cases interm�diaires dans la matrice, et qu'il est int�ressant de stocker les r�sultats interm�diaires.



\section{Algorithme de Needleman et Wunsch}
\label{sec:algoNeedlemanWunsch}

Cet algorithme a �t� pr�sent� en 1970 par Saul Needleman et Christian Wunsch\footnoteURL{https://www.ncbi.nlm.nih.gov/pubmed/5420325}.
Il n�cessite deux param�tres~:
\begin{itemize}
 \item une \emph{p�nalit� de trou} n�gative ou nulle, qui correspond au co�t d'un indel, c'est-�-dire d'un d�placement horizontal ou vertical dans la matrice de comparaison
 \item une \emph{matrice de similarit�} qui est une matrice carr�e dont les lignes et les colonnes correspondent aux lettres de l'alphabet (dans le m�me ordre) et que l'on utilise pour donner un score � un d�placement oblique dans la matrice de comparaison. La diagonale de la matrice de similarit� correspond aux matchs et comporte des scores positifs. Les autres cases correspondent � des substitutions et comportent des scores n�gatifs ou nuls.
 \end{itemize}

\fixme{donner une matrice de similarit� simple et une matrice de similarit� plus complexe.}

Pour les cas simples, on peut fixer par exemple le gain de match � $+5$, et le co�t de substitution � $-3$, ce qui donne la matrice suivante. 
\fixme{diagonale � $+5$ et toutes les autres cases � $-3$}

Remarquez que l'on n'est pas oblig�s d'attribuer un gain de match identique pour toutes les lettres, et que l'on peut souhaiter favoriser les matchs sur les C ou les G m�me si cela doit induire les substitutions ou des indels ailleurs.
De m�me, on peut souhaiter nuancer les substitutions en repr�sentant le fait qu'il est moins grave de remplacer un C par un G plut�t que par un T ou un A.
C'est surtout int�ressant lorsque l'on compare des s�quences d'acides amin�s, pour repr�senter les familles d'acides amin�s qui ont les m�mes propri�t�s physico-chimiques, et dont on peut penser que la substitution aura un impact moins important sur la fonction de la prot�ine\footnoteURL{https://en.wikipedia.org/wiki/BLOSUM}.


\subsection{Intuition}

\fixme{Conserver la section �~Intuition~� ?}

On cherche � comparer les deux s�quences suivantes~:\\
Seq1 : \texttt{ACCGGTAACCGGTTAACACCCAC} \\
Seq2 : \texttt{ACCGGTGGAACCGGTAACACCCAC} 

Le co�t d'un indel sera 0.

La matrice de similarit� sera la matrice identit� (un match donne un gain de $+1$ et une substitution a une p�nalit� de $0$).

La matrice de comparaison sera~:
\begin{center}
\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
\hline
0 &A&C&C&G&G&T&G&G&A&A&C&C&G&G&T&T&A\\
\hline
A&1&& & & & & & &1&1& & & & & & &1\\
\hline
C& &2& & & & & & & & & & & & & & &\\
\hline
C& & & & & & & & & & & & & & & & &\\
\hline
G& & & & & & & & & & & & & & & & &\\
\hline
G& & & & & & & & & & & & & & & & &\\
\hline
T& & & & & & & & & & & & & & & & &\\
\hline
A& & & & & & & & & & & & & & & & &\\
\hline
A& & & & & & & & & & & & & & & & &\\
\hline
C& & & & & & & & & & & & & & & & &\\
\hline
C& & & & & & & & & & & & & & & & &\\
\hline
G& & & & & & & & & & & & & & & & &\\
\hline
G& & & & & & & & & & & & & & & & &\\
\hline
T& & & & & & & & & & & & & & & & &\\
\hline
T& & & & & & & & & & & & & & & & &\\
\hline
A& & & & & & & & & & & & & & & & &\\
\hline
\end{tabular}
\end{center}


La matrice de comparaison se remplit de la fa�on suivante :
\begin{itemize}
\item On part de la case en haut � gauche (celle qui vaut 0) et on se d�place en diagonale vers la droite et vers le bas (cf. point suivant)
\item Pour un d�placement en diagonal, ajoutez la valeur donn�e par la matrice de similarit� � la valeur de la case de d�part, et reportez le total dans la case d'arriv�e.
\item Pour un d�placement vers la droite ou vers le bas, mettez la valeur de la case au co�t d'indel (ici $0$).
\end{itemize}
 
Une fois remplie, nous cherchons les chemins de poids le plus grand. C'est � dire que nous cherchons les endroits dans la matrice qui maximisent les d�placements en diagonale. Il faut alors rep�rer les nombres les plus grands dans la matrice.
\newline
Une fois identifi�s, nous partons de la case la plus en bas � droite de la diagonale et nous la remontons en arri�re pour transcrire l'alignement en �crivant les caract�res de chacune des deux s�quences � chaque ligne et colonne correspondant � la diagonale que vous suivez.
\newline
Lorsque nous rencontrons une coupure dans la diagonale, nous trouvons la diagonale de poids le plus grand qui commence dans une cellule qui est en haut ou � gauche de la cellule actuelle.
\newline 
Pour chaque d�placement vers le haut (non en diagonale), vous devez ins�rer un espace dans la s�quence de l'axe horizontal de votre matrice. Pour chaque d�placement vers la gauche, ins�rez un espace dans la s�quence de l'axe vertical de votre matrice.\\
\vfill
	\textbf{Exemple :}
    \begin{itemize}
    \item D�placement en diagonal = les lettres se font face (match ou mismatch)
    \end{itemize}
    
\begin{center}
\includegraphics{./figures/match.png}
\end{center}

    Alignement :
\newline
      AAT
\newline
        AAT
\begin{itemize}
 \medbreak
\item D�placement vers le haut = insertion d'un '-' dans la s�quence horizontale.
\end{itemize}
\begin{center}
\includegraphics[]{./figures/haut.png}
\end{center}

Alignement :
\newline
AA-G
\newline
AATG

\begin{itemize}
\item D�placement vers la droite = insertion d'un '-' dans la s�quence verticale.
\end{itemize}
\begin{center}
\includegraphics[]{./figures/haut_titre.png}
\end{center}

Alignement :
\newline
AATG
\newline
AA-G
\medbreak


\begin{etape}
 Remplissez la matrice pr�c�dente, vous pouvez simplement indiquer les matchs dans la matrice.
 Identifiez le(s) chemin(s) de plus grand poids.
 D�terminez le(s) meilleur(s) alignement(s).
\end{etape}


\subsection{Principe}

L'alignement que nous venons de construire �tait un \emph{alignement global}, ce qui signifie que nous alignons les deux s�quences de leur d�but � leur fin. 

L'algorithme de Needleman et Wunsch est bas� sur le m�me principe. 
Il divise le probl�me d'alignement global en une s�rie de sous-probl�mes de taille inf�rieure.
Il est aussi connu sur les noms : \textit{optimal matching algorithm} et \textit{global alignment technique}.

Nous comparerons deux s�quences :\\
Seq1 = \texttt{ATCGAACCTG} \\
Seq2 = \texttt{ATGAATG}
\medbreak

\subsubsection{D�finition du co�t des diff�rents �v�nements}

\begin{itemize}
\item Match = 3
\item Mismatch = -1
\item Indel (insertion ou del�tion)= -2
\end{itemize}


\begin{etape}
\fixme{Transposer en scala}
 \begin{itemize}
\item Cr�ez un package \textbf{align} dans Eclipse.
\item Cr�ez une nouvelle classe \textbf{alignement}.
\item Importez le package \textbf{java.util.Arrays}.
\item Cr�ez trois variables static correspondant aux match, mismatch et indel.
\item Attribuez-leur les valeurs correspondantes.
\item Cr�er votre \textbf{main} de la fa�on suivante :
\end{itemize}

\begin{minted}[linenos,fontsize=\small]{java}
public static void main(String[] args){
	 String S = "ATCGAACCTG";
	 String T = "ATGAATG";
} 
\end{minted}
\end{etape}



\subsubsection{Cr�ation de la matrice}
Contrairement � la matrice na�ve les matrices de Needleman-Wunch contiennent une ligne et colone dites d'initialisation.

Caract�ristiques de la matrice :
\begin{itemize}
\item Nombre colonne = Taille(seq1)+1
\item Nombre ligne = Taille(seq2)+1
\end{itemize}


\begin{center}
\begin{tabular}{c|c|c|c|c|c|c|c|c|c|c|c|}
\multicolumn{2}{c}{~~~~~~~~~} & \multicolumn{1}{c}{A} & \multicolumn{1}{c}{T} & \multicolumn{1}{c}{C} & \multicolumn{1}{c}{G} & \multicolumn{1}{c}{A} & \multicolumn{1}{c}{A} & \multicolumn{1}{c}{C} & \multicolumn{1}{c}{C} & \multicolumn{1}{c}{T} & \multicolumn{1}{c}{G}\\
%\hline
% &&A&T&C&G&A&A&C&C&T&G\\
%\hline
\cline{2-12}
 & & & & & & & & & & & \\
%\hline
\cline{2-12}
A& & & & & & & & & & & \\
%\hline
\cline{2-12}
T& & & & & & & & & & & \\
%\hline
\cline{2-12}
G& & & & & & & & & & & \\
%\hline
\cline{2-12}
A& & & & & & & & & & & \\
%\hline
\cline{2-12}
A& & & & & & & & & & & \\
%\hline
\cline{2-12}
T& & & & & & & & & & & \\
%\hline
\cline{2-12}
G& & & & & & & & & & & \\
%\hline
\cline{2-12}
\end{tabular}
\end{center}


% \begin{tabular}{c c|c c c c|}
% \multicolumn{2}{c}{} & Piano & Trumpet & Flute & \multicolumn{1}{c}{Viola}\\
% \multicolumn{2}{c}{}& \textbf{pno} & \textbf{tpt} & \textbf{flt} & \multicolumn{1}{c}{\textbf{vla}}\\
% \cline{3-6}
% Piano & \textbf{pno} & 0 & 0.5 & 0.25 & 0.75\\
% Trumpet & \textbf{tpt} & 0 & 0 & 0.25 & 0.5\\
% Flute & \textbf{flt} & 0 & 0 & 0 & 0.75\\
% Viola & \textbf{vla} & 0& 0 & 0 & 0\\
% \cline{3-6}
% \end{tabular}


\begin{etape}
 Cr�er une fonction \textbf{initmatrix} qui construit une matrice comme ci-dessus.
\begin{minted}[linenos,fontsize=\small]{java}
public static int[][] initMatrix(String S, String T) {
	 //code de creation d'une matrice
	 return matrix;
 }
\end{minted}
\end{etape}



\subsubsection{Initialisation de la matrice}

\begin{center}
\begin{tabular}{c|c|c|c|c|c|c|c|c|c|c|c|}
\multicolumn{2}{c}{~} & \multicolumn{1}{c}{A} & \multicolumn{1}{c}{T} & \multicolumn{1}{c}{C} & \multicolumn{1}{c}{G} & \multicolumn{1}{c}{A} & \multicolumn{1}{c}{A} & \multicolumn{1}{c}{C} & \multicolumn{1}{c}{C} & \multicolumn{1}{c}{T} & \multicolumn{1}{c}{G}\\
%\hline
% &&A&T&C&G&A&A&C&C&T&G\\
%\hline
\cline{2-12}
&0&-2&-4&-6& & & & & & & \\
%\hline
\cline{2-12}
A&-2& & & & & & & & & & \\
%\hline
\cline{2-12}
T&-4& & & & & & & & & & \\
%\hline
\cline{2-12}
G&-6& & & & & & & & & & \\
%\hline
\cline{2-12}
A&-8& & & & & & & & & & \\
%\hline
\cline{2-12}
A& & & & & & & & & & & \\
%\hline
\cline{2-12}
T& & & & & & & & & & & \\
%\hline
\cline{2-12}
G& & & & & & & & & & & \\
%\hline
\cline{2-12}
\end{tabular}
\end{center}




\begin{etape}
 \begin{itemize}
\item Remplir la premi�re ligne et premi�re colonne � la main
\item Cr�er une fonction \textbf{initglobal} qui permet de compl�ter la premi�re ligne et la premi�re colonne comme vous l'avez fait � la main ci-dessus.
\end{itemize}
\begin{minted}[linenos,fontsize=\small]{java}
//fonction qui initialise la premiere colonne et ligne
public static int[][] initGlobal( int[][] matrix) {
    //calcul de la premiere ligne
    //calcul de la premiere colonne
    return matrix;
}
\end{minted}
\end{etape}



\subsubsection{Remplissage de la matrice}
\begin{center}
\begin{tabular}{c|c|c|c|c|c|c|c|c|c|c|c|}
\multicolumn{2}{c}{~} & \multicolumn{1}{c}{A} & \multicolumn{1}{c}{T} & \multicolumn{1}{c}{C} & \multicolumn{1}{c}{G} & \multicolumn{1}{c}{A} & \multicolumn{1}{c}{A} & \multicolumn{1}{c}{C} & \multicolumn{1}{c}{C} & \multicolumn{1}{c}{T} & \multicolumn{1}{c}{G}\\
% \hline
%  &&A&T&C&G&A&A&C&C&T&G\\
%\hline
\cline{2-12}
&0&-2& & & & & & & & & \\
%\hline
\cline{2-12}
A&-2&\textbf{3}& & & & & & & & & \\
%\hline
\cline{2-12}
T& & & & & & & & & & & \\
%\hline
\cline{2-12}
G& & & & & & & & & & & \\
%\hline
\cline{2-12}
A& & & & & & & & & & & \\
%\hline
\cline{2-12}
A& & & & & & & & & & & \\
%\hline
\cline{2-12}
T& & & & & & & & & & & \\
%\hline
\cline{2-12}
G& & & & & & & & & & & \\
%\hline
\cline{2-12}
\end{tabular}
\end{center}

\begin{etape}
\begin{itemize}
\item Remplir la matrice � la main. Pour chaque case pointer � l'aide d'une fl�che la case � partir de laquelle le calcul a �t� fait (voir exemple si dessus). Attention : pour remplir une case il est n�cessaire de connaitre les 3 cases pouvant la pr�c�der.
\item Coder la fonction \textbf{max3} qui compare trois scores et retourne le plus �lev� et qui identifiera le score maximum entre un d�placement en diagonale, vers le bas ou vers la droite.
\begin{minted}[linenos,fontsize=\small]{java}
public static int max3(int a, int b, int c){
	 // comparaison entre trois valeurs et retourne la plus elevee
	 }
\end{minted}


\item Coder la fonction \textbf{score} qui permettra de savoir si le d�placement en diagonale est un match ou un mismatch et de retourner le score
\begin{minted}[linenos,fontsize=\small]{java}
public static int score(char s,char t) {
 	// votre code de comparaison pour savoir s'il s'agit d'un match ou mismatch
 	}
\end{minted}


\item Coder la fonction \textbf{fillNW} qui permet de remplir la matrice et renvoi l'�l�ment trouv� dans la case la plus en bas � droite, en utilisant : \textbf{max3} et \textbf{score} \\

\begin{minted}[linenos,fontsize=\small]{java}
public static int fillNW(int[][] matrix, String S, String T) { 
	for (int i=1; i<=S.length();i++ ) {
		for (int j=1; j<=T.length();j++) {
			//calcul du meilleur score pour la case matrix[i][j]
			}
		}
	return matrix[S.length()][T.length()];
	}
\end{minted}
\end{itemize}
\end{etape}



\subsubsection{D�tection du meilleur alignement}

Pour d�terminer le meilleur alignement, on part de la case la plus bas � droite, on suit ensuite les fl�ches pour remonter la matrice, tout en retranscrivant l'alignement. 

\begin{etape}
 \begin{itemize}
\item D�terminer le meilleur alignement � la main.
\item Que fait la fonction \textbf{whichmax3} ?
\item A quoi correspondent a,b,c ?
\end{itemize}
\medbreak
\begin{minted}[linenos,fontsize=\small]{java}
public static int whichMax3(int a, int b, int c) {
	if (a>b) {
		if (a>c) {return 1;}
		else {return 3;}
		}
	else {
		if (b>c) {return 2;}
		else {return 3;}
		}
	}
\end{minted}
\end{etape}


\begin{etape}
 \begin{itemize}
\item Compl�ter la fonction \textbf{GetAln}, qui va chercher et afficher le meilleur alignement global.
\item Vous afficherez l'alignement selon le mod�le suivant : \\
\end{itemize}
\begin{center}
\begin{tabular}{|c|c|c|c|c|}
\hline
Sequence&match&mismatch&gap S&gap T\\
\hline
S&T&A&T&-\\
\hline
matching&|&.& & \\
\hline
T&T&C&-&G\\
\hline
\end{tabular}
\end{center}

\begin{minted}[linenos,fontsize=\small]{java}
public static void getAln(int[][] matrix, String S, String T) {
	String alS="";
	String alT="";
	String matching="";
	int i =S.length();
	int j = T.length();
	
	while (i>0 && j>0) {
		int coming=whichMax3(matrix[i-1][j]+INDEL,
                                    matrix[i][j-1]+INDEL,
                                    matrix[i-1][j-1]+score(S.charAt(i-1),T.charAt(j-1)));
		if (coming==3) {
            //Actualiser als,alT et matching
		}
		else if (coming==1) {
    		//Actualiser als,alT et matching
		}
		else {
    		//Actualiser als,alT et matching
		}
	}
	
    //commentaire : gap dans T si T>S
	while (i>0) {
		//alT=T.charAt(j-1)+alT;
		alT=T.charAt(i-1)+alT;
		alS="-"+alS;
		i=i-1;
		matching=" "+matching;
	}
	
    //commentaire : gap dans S si S>T
	while (j>0) {
		//alT=T.charAt(j-1)+alT;
		//alS="-"+alS;
		alS=S.charAt(j-1)+alS;
		alT="-"+alT;
		j=j-1;
		matching=" "+matching;
	}
	//imprimer alS, matching et alT
}
\end{minted}
\end{etape}


\begin{etape}
 Compl�ter le \textbf{main} et r�aliser l'alignement de S et T.
\end{etape}


\begin{etape}
 \textsc{(optionnelle)} Coder une fonction \textbf{printMatrix} qui r�alise une sortie graphique de la matrice.
\end{etape}


\fixme{Ajouter un fichier scala dans le d�p�t gitlab}


\chapter{Local sequence alignment: Smith and Waterman}
\label{chap:localAlignment}

\section{Principe}
\label{sec:localAlignmentPrinciple}

Avec l'alignement des s�quences locales, vous n'�tes pas contraint d'aligner l'ensemble des deux s�quences comme vous l'avez r�alis� avec l'alignement global; vous pouvez simplement utiliser des parties de chacune des s�quences pour obtenir un score maximum. Ainsi si on utilise deux s�quences S1 et S2 et la m�me grille de notation que vous avez vu la semaine derni�re, vous obtenez l'alignement local optimal S1'' et S2'' suivant~:

\begin{verbatim}
S1 = GCCCTAGCG
S2 = GCGCAATG

S1   = GCCCTAGCG
S1'' =       GCG
S2'' =       GCG
S2   =       GCGCAATG
\end{verbatim}


Dans l'algorithme Smith-Waterman, votre alignement local n'a pas besoin de se terminer � la fin de l'une ou de l'autre s�quence, donc vous n'avez pas besoin de commencer votre traceback dans le coin inf�rieur droit; on le d�marre dans la cellule avec le score le plus �lev�.
\medbreak

L'algorithme Smith-Waterman diff�re de l'algorithme Needleman-Wunsch en trois points :
\begin{itemize}
\item Dans l'�tape d'initialisation, les cellules de la premi�re ligne et de la premi�re colonne sont toutes remplies de z�ros. 
\item Lorsque vous remplissez le tableau, si le score d'une case devient n�gatif, vous mettez 0 � la place, et vous ajoutez le pointeur en arri�re seulement pour les cellules qui ont des scores positifs.
\item Dans le tra�age (traceback), vous commencez par la cellule qui a le score le plus �lev� et retournez � la case pr�c�dente jusqu' � ce que vous atteigniez une cellule avec un score de 0. 
\end{itemize}	
� part �a, le tra�age fonctionne exactement comme dans l'algorithme Needleman-Wunsch.
La figure~\ref{fig:localAlignmentSmithWatermanExample} r�sume les diff�rents points.

\begin{figure}[h!]
 \centering
 \includegraphics[width=8cm]{figures/SWTab.png}
 % SWTab.png: 580x444 px, 120dpi, 12.28x9.40 cm, bb=0 0 348 266
 \caption{Alignement local~: exemple de fonctionnement de l'algorithme Waterman-Smith (match : 1, mismatch : -1, d�l�tion : -2)}
 \label{fig:localAlignmentSmithWatermanExample}
\end{figure}

\begin{etape}
 R�utilisez et inspirez-vous des fonctions d'alignement global (Chapitre~\ref{chap:globalAlignment}) pour :
\begin{itemize}
\item construire la matrice~;
\item initialiser la matrice~;
\item coder une nouvelle fonction qui va remplir les cellules~;
\item coder une nouvelle fonction traceback.
\end{itemize}
\end{etape}



\section{Limitations}

\subsection{Passage � l'�chelle}

Pour tout alignement, on cr�e une matrice de taille (taille de s�quence 1*taille de s�quence 2). 


\begin{etape}
 Alignez localement les s�quences contenues dans les fichiers \texttt{gene1.txt} et \texttt{gene2.txt}.
 Que constatez-vous~?
\end{etape}



\subsection{Toutes les substitutions ne se valent pas~: matrices de substitution}

L'alignement des s�quences prot�iques est plus complexe que pour les s�quences nucl�iques. 
Il existe 22 acides amin�s chez l'Homme, on ne travaille donc plus sur un dictionnaire � 4 lettres mais � 22. 
Certains de ses acides amin�s ont des propri�t�s physico-chimiques proches (cf. figure~\ref{fig:aminoAcidsCategories}).

On voit notamment qu'une arginine (R) a plus de propri�t�s communes avec une lysine (K) qu'avec une proline (P).
Une mutation qui entra�nerait la substitution d'un R par un K pourrait ainsi avoir moins de cons�quences sur la prot�ine qu'une substitution d'un R par un P.

Lors de l'alignement de s�quences, plut�t que d'appliquer la m�me p�nalit� quelle que soit la substitution, on peut avoir recours � une \emph{matrice de substitution}\footnote{\url{https://en.wikipedia.org/wiki/Substitution_matrix}} (aussi appel�e \emph{matrice de similarit�}) pour donner des scores diff�rents en fonction des acides amin�s.
Il existe plusieurs familles de matrices de substitution~: notamment les PAM\footnote{PAM~: \textit{Point Accepted Mutation}} qui sont des matrices de distances (plus le score est grand, plus les acides amin�s sont diff�rents) et les BLOSUM\footnote{BLOSUM~: \textit{BLOcks SUbstitution Matrix}} qui sont des matrices de similarit� (plus le score est grand, plus les acides amin�s se ressemblent).
La figure~\ref{fig:substitutionMatrixBLOSUM62} montre la matrice de substitution BLOSUM62.
Vous remarquerez notamment que conform�ment au paragraphe pr�c�dent, la substitution d'un R par un K a un score de $+2$ alors que la substitution d'un R par un P a un score de $-2$.
Vous remarquerez �galement sur la diagonale que l'on peut donner des scores diff�rents � des matchs, ce qui peut avoir des cons�quences sur l'alignement comme par exemple favoriser un match de tryptophane � une position plut�t que deux matchs d'isoleucine ailleurs.


\begin{figure}[h!]
 \centering
 \includegraphics[width=12cm]{figures/aminoAcidsCategories.png}
 % aminoAcidsCategories.png: 702x393 px, 96dpi, 18.57x10.40 cm, bb=0 0 526 295
 \caption{Diagramme de Venn des propri�t� physico-chimiques des principaux acides amin�s}
 \label{fig:aminoAcidsCategories}
\end{figure}

\begin{figure}[h!]
 \centering
 \includegraphics[width=12cm]{figures/substitutionMatrixBLOSSUM62.png}
 % substitutionMatrixBLOSSUM62.png: 512x283 px, 120dpi, 10.84x5.99 cm, bb=0 0 307 170
 \caption{Matrice de substitution BLOSUM62}
 %\caption{Substitution matrix BLOSUM62}
 \label{fig:substitutionMatrixBLOSUM62}
\end{figure}


\begin{etape}
 \textsc{(Optionnelle)} Modifiez votre code pour exploiter une matrice de substitution de la famille BLOSUM.
\end{etape}

\fixme{Mettre une matrice dans le rep. \texttt{data}}



\section{Blast : basic local alignment search tool}
\label{sec:Blast}

\subsection{Principe}
\label{sec:BlastPrinciples}

Blast est un outil pour aligner localement des s�quences mais il utilise une heuristique diff�rente et plus optimis�e. 
Cette heuristique permet notamment d'aligner une s�quence face � une base de donn�e qui contient une grande quantit� de s�quence.

Blast fonctionne en plusieurs �tapes~:
\begin{itemize}
 \item \textbf{D�coupage de la s�quence en k-mer}\\
Le terme k-mer se r�f�re � tous les �~sous mots~�(substring) possible de longueur $k$ contenus dans une cha�ne de caract�res. Par exemple, les 3-mers de la s�quence ATCGATG sont ATC,TCG,\\CGA,GAT,ATG.
 \item \textbf{�num�ration de tous les mots correspondants possibles}\\
Tout les k-mers de taille $k$ possibles (AAA,ATA,ACT...) sont align�s avec chaque k-mer de notre s�quence d'int�r�t. \\
Les k-mers qui ont des scores sup�rieurs � un seuil fix� sont gard�s dans la suite des �tapes. Pour l'exemple avec $k=3$ �a semble inutile, mais gardez � l'esprit qu'on utilise plut�t des valeurs de $k$ de l'ordre de $30$.
 \item \textbf{Organisation des k-mers ayant obtenu le plus de points dans un arbre de recherche efficace}
 \item \textbf{Recherche dans la base de donn�e les s�quences poss�dant des matchs exacts avec nos k-mers retenus}
 \item \textbf{Extension du matchs exacts dans la s�quence de la base de donn�es}\\
BLAST va alors essayer de voir si cette r�gion homologue s'�tend au-del� du k-mer de d�part. Il va alors essayer d'�tendre en amont et en aval du k-mer pour voir si le score d'homologie augmente avec cette tentative d'extension. \\
Si les deux s�quences pr�sentent effectivement une homologie locale autour du k-mer de d�part, l'extension va conduire � une augmentation effective du score, car de nouveaux nucl�otides vont se trouver align�s. Si au contraire la tentative d'extension ne permet pas d'augmenter le score, parce que l'homologie ne continue pas, BLAST s'arr�te. Si le score final apr�s extension est sup�rieur � un seuil donn�, l'alignement est conserv� pour l'analyse finale.
 \item \textbf{Analyse du score et �valuation de la pertinence}\\
La recherche exhaustive avec BLAST retourne en g�n�ral plusieurs dizaines d'alignements avec la s�quence d'int�r�t. Cependant, on ne peut rejeter l'hypoth�se que ces r�sultats soient dus au hasard (les bases de donn�es contiennent �norm�ment de s�quences). BLAST �value ses alignements en analysant la distribution des scores d'alignement entre la s�quence d'int�r�t et la banque. Il ajuste cette distribution � une fonction de densit� th�orique, ce qui lui permet de calculer la probabilit� et l'esp�rance math�matique de trouver un alignement donnant un score donn� dans la banque, uniquement du fait du hasard. Les param�tres de cette fonction de densit� varient en fonction des compositions en nucl�otides ou acides amin�s de la s�quence et de la banque analys�e.
\end{itemize}



\subsection{Diff�rentes variantes de Blast}
\label{sec:BlastTypes}

Diff�rents outils blast existent et sont optimis�s pour certains types de donn�es ou pour r�aliser des alignemnents particuliers.
\begin{itemize}
\item \textbf{blastn} : alignement de nucl�otides, s�quence nucl�otidique contre une base de donn�es de s�quences nucl�otidiques.
\item \textbf{blastp} : alignement de prot�ines, s�quence de prot�ine contre une base de donn�es de s�quences de prot�ines.
\item \textbf{blastx} : alignement de s�quence nucl�otidique traduite en s�quence de prot�ine contre une base de donn�es de s�quences de prot�ines.
\item \textbf{tblastn} : alignement de s�quence de prot�ine contre une base de donn�es de s�quences nucl�otidiques traduites en s�quences de prot�ines
\item \textbf{tblastx} : alignement de s�quence nucl�otidique traduite en s�quence de prot�ine contre une base de donn�es de s�quences nucl�otidiques traduites en s�quences de prot�ines.
\end{itemize}



\subsection{Utilisation de Blast en ligne}
\label{sec:BlastOnline}

Nous allons maintenant utiliser Blast en ligne et voir les possibilit�s qu'il y a derri�re en terme d'anlyse.\\
\begin{itemize}
\item Allez sur \url{https://blast.ncbi.nlm.nih.gov/Blast.cgi}, et choisissez \textbf{blastn}
\item Chargez le fichier \texttt{sequence-unknown.txt} dans �~\textit{Enter Query Sequence}~�.
\item Dans �~\textit{Program selection}~�, choisissez \textbf{blastn}.
\item Activez l'option �~\textit{Show results in a new window}~�.
\item Lancez le BLAST.
\item Quel r�sultat obtenez vous ? � quoi semble correspondre la s�quence ?
\item � quoi correspond le max score,total score, la query cover, l'e-value et l'identit� ? (Aidez-vous de l'aide en ligne de blast)
\item Retournez sur la page indiqu�e pr�c�demment et cliquez sur \textbf{blastx}.
\item Lancez une requ�te avec la \texttt{sequence-unknown.txt}
\item � quelles informations suppl�mentaires avez vous acc�s ?
\item Combien de domaines d'activit�s sont r�f�renc�s ? Dans quelle r�gion ?
\\
\end{itemize}

\textbf{Bonus :} Il est possible de modifier plusieurs param�tres dans les diff�rents outils de BLAST (dans Algorithm parameters). Modifiez-les et observez s'il y a des diff�rences.


\end{document}
